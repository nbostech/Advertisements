package com.wavelabs.confirmation.service;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import com.wavelabs.modal.Advertisement;
import com.wavelabs.modal.User;

public class MailConfirmation {

	private static Properties props = null;
	private static MailConfirmation mail = null;
	private static final String htmlTemplateStart = "<html></head> " + "<title>Confirmation from wavelabs</title>"
			+ "<style> body { background-image : url(\"wavelabs.jpg\"); } </style> </head>" + "<body>";
	private static final String htmlTemplateEnd = "</body></html>";

	private MailConfirmation() throws FileNotFoundException, IOException {
		props = new Properties();
		props.load(new FileReader("E:/RestFul/Search/src/main/resources/mail.properties"));
	}

	public static MailConfirmation getInstance() throws FileNotFoundException, IOException {

		if (mail == null) {
			mail = new MailConfirmation();
		}
		return mail;
	}

	public static void sendMail(final String from, final String password, String to, String sub, String msg)
			throws FileNotFoundException, IOException {
		getInstance();
		Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(from, password);
			}
		});
		try {
			MimeMessage message = new MimeMessage(session);
			message.setContent(msg, "text/html");
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
			message.setSubject(sub);
			/* message.setText(msg); */
			Transport.send(message);
			System.out.println("message sent successfully");
		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}

	public static void sendMail(Advertisement advert, User user, String type)
			throws FileNotFoundException, IOException {
		String msg = "<h1 style=\"color:green\"> Hello,  " + advert.getUser().getName() + " </h1>";
		String message = "<h1 style=\"color:red\"> Advertisement posted Successfully </h1>";
		if (type.equals("POST")) {
			message = msg + message + advertisementMessage(advert);
		} else if (type.equals("APPLY")) {

			message = msg + "<h2 style=\"color:red\">" + user.getName() + " Intrested in your advertisement" + "</h2>";
			message = message + userMessage(user);
			message = message + " For Add \n" + advertisementMessage(advert);
		}
		sendMail("gopikrishnagurram279", "AxisAmma@nemanarmy", advert.getUser().getEmail(), "Wavelabs Update",
				htmlTemplateStart + message + htmlTemplateEnd);
	}

	public static String advertisementMessage(Advertisement advert) {

		String message = "";
		message = message + "<table style=\"color:blue\"><tr> Name : " + advert.getName() + "</tr> <tr> Type : "
				+ advert.getType() + " </tr> <tr> Descrption : " + advert.getDescription() + "</tr> <tr> Location :"
				+ advert.getLocation() + " </tr> </table>";
		return message;
	}

	public static String userMessage(User user) {
		return "<table style=\"color:green\"> <tr> Email : " + user.getName() + " </tr> <tr> Phone :" + user.getPhone()
				+ " </tr> </table>";
	}
}
