package com.wavelabs.dao;


import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.wavelabs.modal.User;
import com.wavelabs.modal.UserAdvertIntrested;
import com.wavelabs.utility.Helper;

public class UserAdvertIntrestedDao {

	public static boolean persistUserAdverIntrested(UserAdvertIntrested uai) {
		Session session = Helper.getSession();

		try {
			Transaction tx = session.beginTransaction();
			session.save(uai);
			tx.commit();
			return true;
		} catch (Exception e) {
			return false;
		} finally {
			session.close();
		}

	}

	public static UserAdvertIntrested get(int id) {
		try {
			Session session = Helper.getSession();
			UserAdvertIntrested uai = (UserAdvertIntrested) session.get(UserAdvertIntrested.class, id);
			return uai;
		} catch (Exception e) {

			return null;
		}
	}
	@SuppressWarnings("unchecked")
	public static List<UserAdvertIntrested> getUserAdvertIntrested(int id) {
		Session session = Helper.getSession();
		String hql = "from " + UserAdvertIntrested.class.getName()+" where user=:user";
		User user = (User) session.get(User.class, id); 
		Query query = session.createQuery(hql);
		query.setParameter("user", user);
		List<UserAdvertIntrested> listOfUserIntrests = query.list();
		return listOfUserIntrests;
	}
	public static void main(String[] args) {
		
		List<UserAdvertIntrested> list = getUserAdvertIntrested(8);
		for(UserAdvertIntrested uai : list) {
			System.out.println(uai.getAdvert().getDescription());
		}
		
	}
}
