package com.wavelabs.dao;

import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import com.wavelabs.modal.Advertisement;
import com.wavelabs.modal.Comment;
import com.wavelabs.utility.Helper;

public class CommentDAO {

	public static Boolean persistComment(Comment comment) {
		Session session = Helper.getSession();
		try {
			session.save(comment);
			session.beginTransaction().commit();
			return true;
		} catch (Exception e) {
			return false;
		} finally {
			session.close();
		}
	}

	@SuppressWarnings("unchecked")
	public static Comment[] getAdvertisementComments(int id) {
		Session session = Helper.getSession();
		try {
			Advertisement advertisement = (Advertisement) session.get(Advertisement.class, id);
			String hql = "from " + Comment.class.getName() + " where advertisement=:advertisement";
			Query query = session.createQuery(hql);
			query.setParameter("advertisement", advertisement);
			List<Comment> listOfComments = query.list();
			Comment[] arrayOfComments = new Comment[listOfComments.size()];
			int j = 0;
			for (Comment comment : listOfComments) {
				comment.getAdvertisement().setUser(null);
				arrayOfComments[j++] = comment;
			}
			return arrayOfComments;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}

	public static Comment updateComment(Comment comment) {

		Session session = Helper.getSession();
		try {
			session.update(comment);
			session.beginTransaction().commit();
			return comment;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}

	}

	public static Boolean deleteComment(int id) {

		Session session = Helper.getSession();
		try {
			Comment comment = (Comment) session.get(Comment.class, id);
			session.delete(comment);
			session.beginTransaction().commit();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			session.close();
		}
	}
}
