package com.wavelabs.dao;

import org.hibernate.Session;

import com.wavelabs.modal.User;
import com.wavelabs.utility.Helper;

public class UserDAO {
	public static User getUser(int id) {
		Session session = Helper.getSession();
		return (User) session.get(User.class, id);
	}
}
