package com.wavelabs.multithread;

import java.io.FileNotFoundException;
import java.io.IOException;

import com.wavelabs.confirmation.service.MailConfirmation;
import com.wavelabs.confirmation.service.PhoneConfirmation;
import com.wavelabs.modal.Advertisement;
import com.wavelabs.modal.User;

public class ConfirmationThread implements Runnable {

	private Advertisement add;
	private User user;
	private String type;

	public ConfirmationThread(Advertisement add, User user, String type) {

		this.add = add;
		this.user = user;
		this.type = type;
	}
	

	@Override
	public void run() {
		synchronized (this) {
			try {
				MailConfirmation.sendMail(add, user, type);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			PhoneConfirmation.sendPhoneConfirmation(add, user, type);
			shutDown();
		}

	}

	@SuppressWarnings("deprecation")
	private void shutDown() {
		Thread.currentThread().stop();

	}

}
