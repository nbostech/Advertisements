package com.wavelabs.job.service;

import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

import com.wavelabs.solr.service.SolrIndexService;

public class SolrJobIndexScheduleService {

	private static Scheduler schedule;

	public static Boolean startSchedule(Integer time) {
		try {
			JobDetail jobDetail = JobBuilder.newJob(SolrIndexService.class).withIdentity("solrJob", "insert").build();
			Trigger trigger = TriggerBuilder.newTrigger().withIdentity("simpleTrigger", "insert")
					.withSchedule(SimpleScheduleBuilder.simpleSchedule().withIntervalInSeconds(time)
							.repeatForever())
					.build();
			schedule = StdSchedulerFactory.getDefaultScheduler();
			schedule.start();
			schedule.scheduleJob(jobDetail, trigger);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public static Boolean stopSchedule() {
		try {
			schedule.shutdown();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

}
