package com.wavelabs.resource;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.wavelabs.modal.Message;
import com.wavelabs.modal.UserAdvertIntrested;
import com.wavelabs.service.UserAdvertIntrestService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(value = "UserIntrest")
@Path("intrest")
public class UserAdvertIntrestedResource {

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation("Persisth the Intrested user in an Advertisement")
	@ApiResponses({ @ApiResponse(code = 200, message = "Intrest sucessfully posted"),
			@ApiResponse(code = 500, message = "Intrest on advertisement posting failed due to server error") })
	public Response persistAdvertIntrest(
			@ApiParam(name = "userIntrestedAdvertisement", required = true) UserAdvertIntrested uia) {
		boolean flag = UserAdvertIntrestService.persistUserAdvert(uia);
		Message messge = new Message();
		if (flag) {
			messge.setId(200);
			messge.setMessage("Your intrest successfully submited");
			return Response.status(200).entity(messge).build();
		} else {
			messge.setId(500);
			messge.setMessage("Not avaliable");
			return Response.status(500).entity(messge).build();
		}
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("{id}")
	@ApiOperation(value = "Gets User intrest advertisement")
	@ApiResponses({ @ApiResponse(code = 200, message = "Returns the UserIntrestedAdvertisement object"),
			@ApiResponse(code = 500, message = "Intrest on advertisement posting failed due to server error"),
			@ApiResponse(code = 404, message = "Not found") })
	public Response getAdvertIntrest(@ApiParam(name = "id", required = true) @PathParam("id") int id) {
		UserAdvertIntrested uai = UserAdvertIntrestService.getUserAdvert(id);
		if (uai != null) {
			return Response.status(200).entity(uai).build();
		} else {
			Message message = new Message();
			message.setId(404);
			message.setMessage("Intrest not found");
			return Response.status(404).entity(message).build();
		}
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("all/{user_id}")
	@ApiOperation(value = "Retrieves the all user intrested advertisements")
	@ApiResponses({ @ApiResponse(code = 200, message = "Returns the UserIntrested Advertisements"),
			@ApiResponse(code = 404, message = "User intrest not found") })
	public Response getUserIntrestedAdverts(
			@ApiParam(name = "user_id", required = true) @PathParam("user_id") Integer id) {

		List<UserAdvertIntrested> uaiList = UserAdvertIntrestService.getAllUserAdvertIntrests(id);
		if (uaiList.size() > 0) {
			GenericEntity<List<UserAdvertIntrested>> genericEntity = new GenericEntity<List<UserAdvertIntrested>>(
					uaiList) {
			};
			return Response.status(200).entity(genericEntity).build();
		} else {
			Message message = new Message();
			message.setId(404);
			message.setMessage("User intrests not found");
			return Response.status(404).entity(message).build();
		}
	}
}
