package com.wavelabs.resource;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

import com.google.gson.Gson;
import com.wavelabs.modal.Advertisement;
import com.wavelabs.modal.Comment;
import com.wavelabs.modal.Message;
import com.wavelabs.service.AdvertisementService;
import com.wavelabs.solr.service.SolrSearchService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Path("/advertisement")
@Provider
@Api(value = "Advertisement", description = "Performs the creation, updation, deletion and reading operations of Adverisement")
public class AdvertisementResource {

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Persists the Advertisement", notes = "Need to pass Advertisement json object")
	@ApiResponses({ @ApiResponse(code = 200, message = "Successfully Advertisement is posted"),
			@ApiResponse(code = 500, message = "Advertisement posting failed due to server error") })
	public Response persistAdvertisement(
			@ApiParam(name = "Advertisement", required = true) Advertisement add) {

		boolean flag = AdvertisementService.persistAdvertisement(add);

		Message message = new Message();
		if (flag) {
			message.setId(200);
			message.setMessage("Operation success");
			return Response.status(200).entity(message).build();
		} else {

			message.setId(500);
			message.setMessage("Failed");
			return Response.status(500).entity(message).build();
		}
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("{id}")
	@ApiOperation(value = "Returns the Advertisement object for given id", notes = "Id should contain integers")
	@ApiResponses({ @ApiResponse(code = 200, message = "Advertisement successfully returned"),
			@ApiResponse(code = 404, message = "Advertisement not found") })
	public Response getAdvertisement(@ApiParam(name = "id", required = true) @PathParam("id") Integer id) {

		Advertisement add = AdvertisementService.getAdvertisement(id);
		if (add != null) {
			return Response.status(200).entity(add).build();
		} else {
			Message message = new Message();
			message.setId(404);
			message.setMessage("Advertisement not found");
			return Response.status(404).entity(message).build();
		}
	}

	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("{id}")
	@ApiOperation(value = "Updates the Advertisement")
	@ApiResponses({ @ApiResponse(code = 200, message = "Successfully updated"),
			@ApiResponse(code = 503, message = "Operation unavaliable"),
			@ApiResponse(code = 500, message = "Internal server error occured") })
	public Response updateAdvertisement(@ApiParam(name = "id", required = true) @PathParam("id") int id,
			@ApiParam(name = "add", required = true) Advertisement add) {

		add.setId(id);
		Advertisement newAdd = AdvertisementService.updateAdvertisement(add);

		if (newAdd != null) {
			return Response.status(200).entity(newAdd).build();
		} else {
			Message message = new Message();
			message.setId(503);
			message.setMessage("Operation not avalible");
			return Response.status(503).entity(message).build();
		}
	}

	@DELETE
	@Path("{id}")
	@ApiOperation(value = "Delethe the Advertisement for given id")
	@ApiResponses({ @ApiResponse(code = 200, message = "Advertisement successfully deleted"),
			@ApiResponse(code = 404, message = "Job not found"),
			@ApiResponse(code = 500, message = "Server error occured") })
	public Response deleteJob(@ApiParam(name = "id", required = true) @PathParam("id") int id) {

		boolean flag = AdvertisementService.deleteAdvertisement(id);
		if (flag) {
			Message message = new Message();
			message.setId(200);
			message.setMessage("Operation success");
			return Response.status(200).entity(message).build();
		} else {
			Message message = new Message();
			message.setId(404);
			message.setMessage("Unavaliable");
			return Response.status(404).entity(message).build();
		}
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("search/{search}")
	@ApiOperation(value = "Searches the advertisement for given Search terms")
	@ApiResponses({ @ApiResponse(code = 200, message = "Returns the Advertisement objects"),
			@ApiResponse(code = 500, message = "Server error occured") })
	public Response getAdvertisements(
			@ApiParam(name = "search", value = "Search Advertisements for given search term") @PathParam("search") String search) {
		Advertisement adds[] = SolrSearchService.getAdverts(search);
		if (adds.length != 0) {
			return Response.ok(200).entity(adds).build();
		} else {
			Message messge = new Message();
			messge.setId(404);
			messge.setMessage("Didn't find the matched search result");
			return Response.ok(500).entity(messge).build();
		}
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("all/{user_id}")
	@ApiOperation("Returns all Advertisements posted by the user")
	@ApiResponses({ @ApiResponse(code = 200, message = "Successfully returns all the advertisements"),
			@ApiResponse(code = 404, message = "Advertisements not found") })
	public Response getAdvertisementsOfUser(@PathParam("user_id") Integer id) {
		List<Advertisement> listOfAdds = AdvertisementService.getAllAdvertisementOfUser(id);
		if (listOfAdds.size() != 0) {

			GenericEntity<List<Advertisement>> adds = new GenericEntity<List<Advertisement>>(listOfAdds) {
			};
			return Response.status(200).entity(adds).build();
		} else {
			Message message = new Message();
			message.setId(404);
			message.setMessage("Advertisements not found");
			return Response.status(200).entity(message).build();
		}
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("comment/{advertisement_id}")
	@ApiOperation("Returns the all comments of Advertisement")
	@ApiResponses({ @ApiResponse(code = 200, message = "successfully returns the all comments for given advertisement"),
			@ApiResponse(code = 404, message = "Comments not found for given advertisement id"),
			@ApiResponse(code = 500, message = "Server error occured") })
	public Response getCommentsOfAdvertisement(@PathParam("advertisement_id") int id) {

		Comment[] comments = AdvertisementService.getListOfCommentsOfAdvertisement(id);
		if (comments.length != 0) {
			/*
			 * GenericEntity<List<Comment>> genericEntity = new
			 * GenericEntity<List<Comment>>(comments) {
			 * 
			 * 
			 * };
			 */
			Gson gson = new Gson();
			String json = gson.toJson(comments);
			return Response.status(200).entity(json).build();
		} else {
			Message message = new Message();
			message.setId(404);
			message.setMessage("No comments");
			return Response.status(404).entity(message).build();
		}
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("comment")
	@ApiOperation(value = "Persist the comment")
	@ApiResponses({ @ApiResponse(code = 200, message = "Persisting of comment is success"),
			@ApiResponse(code = 500, message = "Persisting failed due to server error") })
	public Response persistComment(Comment comment) {

		boolean flag = AdvertisementService.persistComment(comment);
		Message message = new Message();

		if (flag) {
			message.setId(200);
			message.setMessage("Your comment succssfully posted");
			return Response.status(200).entity(message).build();
		} else {
			message.setId(503);
			message.setMessage("This operation currently not possible");
			return Response.status(503).entity(message).build();
		}
	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("comment/{id}")
	@ApiOperation(value = "Updates the comment for given id")
	@ApiResponses({ @ApiResponse(code = 200, message = "Comment successfully updated"),
			@ApiResponse(code = 500, message = "Comment update failed due to the server error") })
	public Response updateComment(@ApiParam(name = "comment", required = true) Comment comment,
			@ApiParam(name = "comment_id", required = true) @PathParam("id") int id) {
		comment.setId(id);
		Comment newComment = AdvertisementService.updateComment(comment);
		if (newComment == null) {
			Message message = new Message();
			message.setId(500);
			message.setMessage("Updation failed due to the server error");
			return Response.status(500).entity(message).build();
		} else {
			return Response.status(200).entity(newComment).build();
		}
	}

	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	@Path("comment/{id}")
	@ApiResponses({ @ApiResponse(code = 200, message = "comment deleted successfully"),
			@ApiResponse(code = 404, message = "comment not found") })
	public Response deleteComment(@ApiParam(name = "comment_id", required = true) @PathParam("id") int id) {

		boolean flag = AdvertisementService.deleteComment(id);
		Message message = new Message();
		if (flag) {
			message.setId(200);
			message.setMessage("Delete successfully");
			return Response.status(200).entity(message).build();
		} else {
			message.setId(404);
			message.setMessage("Not found");
			return Response.status(404).entity(message).build();
		}
	}
}
