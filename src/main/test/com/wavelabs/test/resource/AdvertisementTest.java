package com.wavelabs.test.resource;


import static org.testng.Assert.assertNotNull;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.Assert;
import org.junit.Test;

import com.wavelabs.modal.Advertisement;
import com.wavelabs.modal.User;
import com.wavelabs.resource.AdvertisementResource;

public class AdvertisementTest extends JerseyTest {

	@Override
	public Application configure() {
		return new ResourceConfig(AdvertisementResource.class);
	}

	@Test
	public void testGetAdvertisement() {
		Response response = target("/advertisement/1").request().get();
		assertNotNull(response.getEntity());
		Assert.assertEquals("Phone add", response.readEntity(Advertisement.class).getName());
	}
	
	@Test
	public void testPersistAdvertisement() {
		
		Advertisement add = new Advertisement();
		add.setId(1);
		add.setName("Book for sale");
		add.setDescription("I have 200 pages note book for the price of 20rs only/-");
		add.setLocation("Hyderabad");
		User user = new User();
		user.setId(1);
		user.setName("gopi krishna");
		user.setEmail("gopikrishnagurram279@gmail.com");
		user.setPhone("9032118864");
		add.setUser(user);
		Response response = target("/advertisement").request().post(Entity.json(add));
		Assert.assertEquals(200, response.getStatus());
	}
	
}
